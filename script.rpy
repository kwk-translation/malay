﻿# game/script.rpy:260
translate malay konami_code_e5b259c9:

    # "Max le Fou" "What the heck am I doing here?..."
    "Max le Fou" "Apa jadah aku buat kat sini?..."

# game/script.rpy:297
translate malay gjconnect_4ae60ea0:

    # "Disconnected."
    "Tidak disambung."

# game/script.rpy:326
translate malay gjconnect_e254a406:

    # "Connection to Game Jolt succesful.\nWARNING: The connection doesn't count for previously saved games."
    "Sambungan ke Game Jolt berjaya.\nAMARAN: Sambungan tidak dikira untuk permainan yang disimpan sebelum ini."

translate malay strings:

    # script.rpy:269
    old "Disconnect from Game Jolt?"
    new "Buang sambungan ke Game Jolt ?"

    # script.rpy:269
    old "Yes, disconnect."
    new "Ya, buang sambungan."

    # script.rpy:269
    old "No, return to menu."
    new "Tidak, kembali ke menu."

    # script.rpy:331
    old "A problem occured. Maybe your connection has a problem. Or maybe it's Game Jolt derping...\n\nTry again?"
    new "Telah berlakunya masalah. Mungkin sambungan anda bermasalah. Atau mungkin Game Jolt yang buat bodoh...\n\nCuba lagi?"

    # script.rpy:331
    old "Yes, try again."
    new "Ya, cuba lagi."

    # script.rpy:349
    old "It seems the authentication failed. Maybe you didn't write correctly the username and token...\n\nTry again?"
    new "Nampaknya pengesahan gagal. Mungkin anda tersilap tulis nama pengguna dan token...\n\nCuba lagi?"

    # script.rpy:228
    old "Please enter your name and press Enter:"
    new "Sila masukkan nama anda dan tekan Enter:"

    # script.rpy:295
    old "Type here your username and press Enter."
    new "Tuliskan nama pengguna anda di sini dan tekan Enter."

    # script.rpy:296
    old "Now, type here your game token and press Enter."
    new "Sekarang, tuliskan token permainan anda di sini dan tekan Enter."

    # script.rpy:207
    old "Game Jolt trophy obtained!"
    new "Trofi Game Jolt diperoleh!"

    # script.rpy:177
    old "Sakura"
    new "Sakura"

    # script.rpy:178
    old "Rika"
    new "Rika"

    # script.rpy:179
    old "Nanami"
    new "Nanami"

    # script.rpy:180
    old "Sakura's mom"
    new "Mak Sakura"

    # script.rpy:181
    old "Sakura's dad"
    new "Ayah Sakura"

    # script.rpy:182
    old "Toshio"
    new "Toshio"

    # script.rpy:188
    old "Taichi"
    new "Taichi"

    # dialogs.rpy:199
    old "Master"
    new "Tuan"

    # dialogs.rpy:200
    old "Big brother"
    new "Abang"

    # script.rpy:13
    old "{size=80}Thanks for playing!"
    new "{size=80}Terima kasih sebab main!"
# TODO: Translation updated at 2019-04-23 11:16

# game/script.rpy:286
translate malay update_menu_dc3e4e02:

    # "This functionality is disabled on the Steam version of the game. To use it, use the Game Jolt Version of the game. How did you get here btw? oO"
    "This functionality is disabled on the Steam version of the game. To use it, use the Game Jolt Version of the game. How did you get here btw? oO"

# game/script.rpy:300
translate malay gjconnect_dc3e4e02:

    # "This functionality is disabled on the Steam version of the game. To use it, use the Game Jolt Version of the game. How did you get here btw? oO"
    "This functionality is disabled on the Steam version of the game. To use it, use the Game Jolt Version of the game. How did you get here btw? oO"

# TODO: Translation updated at 2019-04-27 11:11

translate malay strings:

    # script.rpy:226
    old "Achievement obtained!"
    new "Achievement obtained!"

