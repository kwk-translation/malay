# Malay translation of Kare wa Kanojo (Latin script)

**Status of the translation:** 25% - Looking for translators and corrections...


## Notes about this Malay Translation

This repository is for the Malay language written with the Latin script (tulisan Rumi). For the Arabic script (tulisan Jawi), [use this repository here](https://gitlab.com/kwk-translation/malay_arab).

Since Malay is kinda genderless language and it has many levels of pronouns, please follow this guideline:
- Please try to **include gender or use word that hints on gender when necessary** so that the story would make sense, because this is Kare wa Kanojo.
- Because of Rika's personality, please use casual/informal pronoun in all situation even though it's kinda uncommon for girls:
  - Use "aku" instead of "saya" for first-person pronoun.
  - Use "kau" instead of "awak" for second-person pronoun.
- Because of Sakura's personality, please use the more polite/formal pronoun:
  - Use "saya" for first-person pronoun.
  - Use "awak" for second-person pronoun.
- Because of Nanami's personality which speaks like a child, mix in the pronoun:
  - Use "saya" for first-person pronoun in normal situation.
  - Use "aku" for first-person pronoun when she is challenging or any unique action.
  - Use "awak" for second-person pronoun in normal situation.
  - Use "kau" for second-person pronoun when she is challenging or any unique action.
- The player should adjust their speech to the person they're talking to just like Malay in real life, because this is visual novel:
  - If the girl use casual/informal pronoun then the player should use it too.
  - If the girl use polite/formal pronoun then the player should use it too.
- The translation will be reviewed again after the translation reach 100% to eliminate inconsistency.
- Report any errors, mistranslations or inconsistency through [GitLab issues](https://gitlab.com/kwk-translation/malay/issues).

Progress:

- [x] Non-dialogue translations (menu etc.)
- [x] Chapter 1
- [ ] Chapter 2
- [ ] Chapter 3
- [ ] Chapter 4
- [ ] Chapter 5
- [ ] Chapter 6
- [ ] All bonus

Percentage: 25% (2 out of 8)