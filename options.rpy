﻿translate malay strings:

    # options.rpy:14
    old "Kare wa Kanojo"
    new "Kare wa Kanojo"
    # I'm leaving this as is, because pronouns in Malay don't have any gender information.
    # The Malay word "dia" is used for both "he" and "she".
    # Possible alternative is "Lelaki tu perempuan tu" (That boy that girl)
    # But I think the Japanese title is better at this.