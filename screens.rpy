﻿translate malay strings:

    # screens.rpy:253
    old "Back"
    new "Kembali"

    # screens.rpy:254
    old "History"
    new "Sejarah"

    # screens.rpy:255
    old "Skip"
    new "Langkau"

    # screens.rpy:256
    old "Auto"
    new "Auto"

    # screens.rpy:257
    old "Save"
    new "Simpan"

    # screens.rpy:258
    old "Q.save"
    new "C.Simpan"

    # screens.rpy:259
    old "Q.load"
    new "C.Muat"

    # screens.rpy:260
    old "Settings"
    new "Tetapan"

    # screens.rpy:302
    old "New game"
    new "Permainan baru"

    # screens.rpy:309
    old "Update Available!"
    new "Ada Kemas Kini Terbaru!"

    # screens.rpy:320
    old "Load"
    new "Muatkan"

    # screens.rpy:323
    old "Bonus"
    new "Bonus"

    # screens.rpy:329
    old "End Replay"
    new "Tamatkan Ulang Tayang"

    # screens.rpy:333
    old "Main menu"
    new "Menu utama"

    # screens.rpy:335
    old "About"
    new "Perihal"

    # screens.rpy:337
    old "Please donate!"
    new "Sila derma!"

    # screens.rpy:342
    old "Help"
    new "Bantuan"

    # screens.rpy:346
    old "Quit"
    new "Keluar"

    # screens.rpy:572
    old "Version [config.version!t]\n"
    new "Versi [config.version!t]\n"

    # screens.rpy:578
    old "Ren'Py version: [renpy.version_only].\n\n[renpy.license!t]"
    new "Versi Ren'Py: [renpy.version_only].\n\n[renpy.license!t]"

    # screens.rpy:618
    old "Page {}"
    new "Halaman {}"

    # screens.rpy:618
    old "Automatic saves"
    new "Simpan automatik"

    # screens.rpy:618
    old "Quick saves"
    new "Simpan cepat"

    # screens.rpy:660
    old "{#file_time}%A, %B %d %Y, %H:%M"
    new "{#file_time}%A, %d %B %Y, %H:%M"

    # screens.rpy:660
    old "empty slot"
    new "slot kosong"

    # screens.rpy:677
    old "<"
    new "<"

    # screens.rpy:680
    old "{#auto_page}A"
    new "{#auto_page}A"

    # screens.rpy:683
    old "{#quick_page}Q"
    new "{#quick_page}Q"

    # screens.rpy:689
    old ">"
    new ">"

    # screens.rpy:751
    old "Display"
    new "Paparan"

    # screens.rpy:752
    old "Windowed"
    new "Dalam Tetingkap"

    # screens.rpy:753
    old "Fullscreen"
    new "Skrin Penuh"

    # screens.rpy:757
    old "Rollback Side"
    new "Sisi Undur"

    # screens.rpy:758
    old "Disable"
    new "Lumpuhkan"

    # screens.rpy:759
    old "Left"
    new "Kiri"

    # screens.rpy:760
    old "Right"
    new "Kanan"

    # screens.rpy:765
    old "Unseen Text"
    new "Teks Belum Lihat"

    # screens.rpy:766
    old "After choices"
    new "Lepas Pilihan"

    # screens.rpy:767
    old "Transitions"
    new "Lepas Peralihan"

    # screens.rpy:777
    old "Language"
    new "Bahasa"

    # screens.rpy:807
    old "Text speed"
    new "Kelajuan Tulisan"

    # screens.rpy:811
    old "Auto forward"
    new "Bergerak sendiri"

    # screens.rpy:818
    old "Music volume"
    new "Kekuatan muzik"

    # screens.rpy:825
    old "Sound volume"
    new "Kekuatan bunyi"

    # screens.rpy:831
    old "Test"
    new "Cuba"

    # screens.rpy:835
    old "Voice volume"
    new "Kekuatan suara"

    # screens.rpy:846
    old "Mute All"
    new "Bisukan SEMUA"

    # screens.rpy:965
    old "The dialogue history is empty."
    new "Sejarah dialog kosong."

    # screens.rpy:1030
    old "Keyboard"
    new "Papan Kekunci"

    # screens.rpy:1031
    old "Mouse"
    new "Tetikus"

    # screens.rpy:1034
    old "Gamepad"
    new "Pad Permainan"

    # screens.rpy:1047
    old "Enter"
    new "Enter"

    # screens.rpy:1048
    old "Advances dialogue and activates the interface."
    new "Menggerakkan dialog dan mengaktifkan antara muka."

    # screens.rpy:1051
    old "Space"
    new "Space"

    # screens.rpy:1052
    old "Advances dialogue without selecting choices."
    new "Menggerakkan dialog tanpa memilih pilihan."

    # screens.rpy:1055
    old "Arrow Keys"
    new "Kekunci Anak Panah"

    # screens.rpy:1056
    old "Navigate the interface."
    new "Melayar antara muka."

    # screens.rpy:1059
    old "Escape"
    new "Escape"

    # screens.rpy:1060
    old "Accesses the game menu."
    new "Mencapai menu permainan."

    # screens.rpy:1063
    old "Ctrl"
    new "Ctrl"

    # screens.rpy:1064
    old "Skips dialogue while held down."
    new "Langkau dialog apabila ditekan."

    # screens.rpy:1067
    old "Tab"
    new "Tab"

    # screens.rpy:1068
    old "Toggles dialogue skipping."
    new "Menogol langkauan dialog."

    # screens.rpy:1071
    old "Page Up"
    new "Page Up"

    # screens.rpy:1072
    old "Rolls back to earlier dialogue."
    new "Bergerak ke dialog sebelumnya."

    # screens.rpy:1075
    old "Page Down"
    new "Page Down"

    # screens.rpy:1076
    old "Rolls forward to later dialogue."
    new "Bergerak ke dialog seterusnya."

    # screens.rpy:1080
    old "Hides the user interface."
    new "Menyembunyikan antara muka pengguna."

    # screens.rpy:1084
    old "Takes a screenshot."
    new "Mengambil tangkap skrin."

    # screens.rpy:1088
    old "Toggles assistive {a=https://www.renpy.org/l/voicing}self-voicing{/a}."
    new "Menogol {a=https://www.renpy.org/l/voicing}penyuaraan sendiri{/a} yang membantu."

    # screens.rpy:1094
    old "Left Click"
    new "Klik Kiri"

    # screens.rpy:1098
    old "Middle Click"
    new "Klik Tengah"

    # screens.rpy:1102
    old "Right Click"
    new "Klik Kanan"

    # screens.rpy:1106
    old "Mouse Wheel Up\nClick Rollback Side"
    new "Tatal Ke Atas\nKlik Sisi Undur"

    # screens.rpy:1110
    old "Mouse Wheel Down"
    new "Tatal Ke Bawah"

    # screens.rpy:1117
    old "Right Trigger\nA/Bottom Button"
    new "Picu Kanan\nA/Butang Bawah"

    # screens.rpy:1121
    old "Left Trigger\nLeft Shoulder"
    new "Picu Kiri\nBahu Kiri (L1)"

    # screens.rpy:1125
    old "Right Shoulder"
    new "Bahu Kanan (R1)"

    # screens.rpy:1129
    old "D-Pad, Sticks"
    new "D-Pad (Butang Arah), Kayu Bedik"

    # screens.rpy:1133
    old "Start, Guide"
    new "Butang Start, Guide"

    # screens.rpy:1137
    old "Y/Top Button"
    new "Y/Butang Atas"

    # screens.rpy:1140
    old "Calibrate"
    new "Tentuukur"

    # screens.rpy:1205
    old "Yes"
    new "Ya"

    # screens.rpy:1206
    old "No"
    new "Tidak"

    # screens.rpy:1252
    old "Skipping"
    new "Melangkau"

    # screens.rpy:1473
    old "Menu"
    new "Menu"

    # screens.rpy:1530
    old "Picture gallery"
    new "Galeri gambar"

    # screens.rpy:1534
    old "Music player"
    new "Pemain muzik"

    # screens.rpy:1536
    old "Opening song lyrics"
    new "Lirik lagu pembuka"

    # screens.rpy:1543
    old "Musics and pictures"
    new "Muzik dan gambar"

    # screens.rpy:1508
    old "Bonus chapters"
    new "Bab-bab bonus"

    # screens.rpy:1511
    old "1 - A casual day at the club"
    new "1 - Hari biasa di kelab"

    # screens.rpy:1516
    old "2 - Questioning sexuality (Sakura's route)"
    new "2 - Menanyakan seksualiti (haluan Sakura)"

    # screens.rpy:1521
    old "3 - Headline news"
    new "3 - Berita hangat"

    # screens.rpy:1526
    old "4a - A picnic at the summer (Sakura's route)"
    new "4a - Berkelah di musim panas (haluan Sakura)"

    # screens.rpy:1531
    old "4b - A picnic at the summer (Rika's route)"
    new "4b - Berkelah di musim panas (haluan Rika)"

    # screens.rpy:1536
    old "4c - A picnic at the summer (Nanami's route)"
    new "4c - Berkelah di musim panas (haluan Nanami)"

    # char profiles
    old "Characters profiles"
    new "Profil watak"

# TODO: Translation updated at 2019-04-23 11:16

    # screens.rpy:1611
    old "Max le Fou - Taichi's Theme"
    new "Max le Fou - Taichi's Theme"

    # screens.rpy:1613
    old "Max le Fou - Sakura's Waltz"
    new "Max le Fou - Sakura's Waltz"

    # screens.rpy:1615
    old "Max le Fou - Rika's theme"
    new "Max le Fou - Rika's theme"

    # screens.rpy:1617
    old "Max le Fou - Of Bytes and Sanshin"
    new "Max le Fou - Of Bytes and Sanshin"

    # screens.rpy:1619
    old "Max le Fou - Time for School"
    new "Max le Fou - Time for School"

    # screens.rpy:1621
    old "Max le Fou - Sakura's secret"
    new "Max le Fou - Sakura's secret"

    # screens.rpy:1623
    old "Max le Fou - I think I'm in love"
    new "Max le Fou - I think I'm in love"

    # screens.rpy:1625
    old "Max le Fou - Darkness of the Village"
    new "Max le Fou - Darkness of the Village"

    # screens.rpy:1627
    old "Max le Fou - Love always win"
    new "Max le Fou - Love always win"

    # screens.rpy:1629
    old "Max le Fou - Ondo"
    new "Max le Fou - Ondo"

    # screens.rpy:1631
    old "Max le Fou - Happily Ever After"
    new "Max le Fou - Happily Ever After"

    # screens.rpy:1633
    old "J.S. Bach - Air Orchestral suite #3"
    new "J.S. Bach - Air Orchestral suite #3"

    # screens.rpy:1635
    old "Mew Nekohime - TAKE MY HEART (TV Size)"
    new "Mew Nekohime - TAKE MY HEART (TV Size)"
# TODO: Translation updated at 2019-04-27 11:11

translate malay strings:

    # screens.rpy:1552
    old "Achievements"
    new "Achievements"

